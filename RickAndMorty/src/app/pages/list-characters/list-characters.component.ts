import { CharacterService } from './../../services/character.service';
import { Character, Info } from './../../interfaces/character.interface';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-list-characters',
  templateUrl: './list-characters.component.html',
  styleUrls: ['./list-characters.component.css']
})
export class ListCharactersComponent implements OnInit {
  characters: Character[] = [];
  info: Info[] = [];
  characterTextFC = new FormControl('');

  constructor(private characterService: CharacterService) { }

  ngOnInit(): void {
    this.characterTextFC.valueChanges.subscribe(res =>{
      console.log(res);
      this.searchCharacter(res);
    })
    // this.characters = [];
    this.pagesList();
    this.allCharacters();
    
    
  }

  allCharacters(){
    this.characterService.getCharacters().subscribe(res => {
      console.log(res);
      this.characters = res;
    })
  }

  searchCharacter(characterName: string | null = null){
    this.characterService.getSerchCharacter(characterName).subscribe(res => {
      this.characters = res;
    })
  }

  pagesList(){
    this.characterService.getPages().subscribe(res => {
      console.log(res);
      this.info = res;
    })
  }
}
