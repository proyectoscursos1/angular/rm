import { Character, Info } from './../interfaces/character.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class CharacterService {
    API_URL = 'https://rickandmortyapi.com/api/character';

constructor(private http: HttpClient) { }

getCharacters(){
    return this.http.get<Character[]>(this.API_URL).pipe(map((res: any) => res.results));    
}

getSerchCharacter(name: string | null){
    const params = { name };
    return this.http.get<Character[]>(this.API_URL, { params }).pipe(map((res: any) => res.results));  
}

getPages(){
    return this.http.get<Info[]>(this.API_URL).pipe(map((res: any) => res.info));    
}

}